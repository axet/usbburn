﻿using System;
using System.Collections.Generic;
using System.Text;

namespace usbburn
{
    abstract public class Job
    {
        public struct Info
        {
            public string Name;
            public long Max;
        };

        abstract public Info GetJobInfo();
        abstract public long Poll();
    }
}
