﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Threading;

namespace usbburn.Jobs
{
    class Burn : Job, IDisposable
    {
        Info m_info;

        FileStream m_read;

        LowLevelIO m_io;

        public Burn(LowLevelIO io,string image)
        {
            m_io = io;
            m_read = new FileStream(image, FileMode.Open);

            m_info.Name = "Burn";
            m_info.Max = m_read.Length;
        }

        override public Info GetJobInfo()
        {
            return m_info;
        }

        override public long Poll()
        {
            byte[] buf = new byte[1024*1024];
            int read = m_read.Read(buf, 0, buf.Length);
            m_io.Write(buf);

            return m_read.Position;
        }

        public void Dispose()
        {
            m_read.Close();
        }
    }
}
