﻿using System;
using Microsoft.Win32.SafeHandles;
using System.Collections.Generic;
using System.Windows.Forms;

namespace usbburn
{
    static class Program
    {
        static UsbBurn m_mainform = null;

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            try
            {
                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);

                m_mainform = new UsbBurn();
                Application.Run(m_mainform);
            }
            catch (Exception er)
            {
                ReportException(er);
            }

        }

        public static void ReportException(Exception e)
        {
            MessageBox.Show(m_mainform, e.ToString(), Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
        }
    }
}
