﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("USBBurn")]
[assembly: AssemblyDescription("Small utility for writing disk images to usb drive. Project created with Microsoft Visual Studio 2008 C#. http://www.codeplex.com/usbburn")]
[assembly: AssemblyProduct("USBBurn")]
[assembly: AssemblyCopyright("Copyright © 2008 Alexey Kuznetsov mailto:ak@axet.ru")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("45255770-c07a-4466-b2ec-11d767fe700b")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers 
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("1.0.0")]
[assembly: AssemblyFileVersion("1.0.0")]
