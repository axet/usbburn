﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace usbburn
{
    public partial class UsbBurn : Form
    {
        const int WM_USER = 0x0400;
        const int WM_ABOUTBOX = WM_USER + 1;

        SystemMenu m_sm;

        public UsbBurn()
        {
            InitializeComponent();

            m_sm = new SystemMenu(this);
            m_sm.AppendSeparator();
            m_sm.AppendMenu(WM_ABOUTBOX, "About...");
        }

        private void burn_Click(object sender, EventArgs e)
        {
            LowLevelIO l = null;
            Jobs.Burn b = null;
            try
            {
                l = new LowLevelIO(textBox1.Text, FileAccess.Write);
                b = new usbburn.Jobs.Burn(l, textBox2.Text);
                Progress p = new Progress();
                p.AddJob(b);
                p.ShowDialog(this);
            }
            catch (Exception ee)
            {
                Program.ReportException(ee);
            }
            finally
            {
                if (l != null) 
                    l.Dispose();
                if (b != null)
                    b.Dispose();
            }
        }

        private void image_Click(object sender, EventArgs e)
        {
            OpenFileDialog f = new OpenFileDialog();

            try
            {
                f.InitialDirectory = Path.GetDirectoryName(textBox2.Text);
            }
            catch (ArgumentException)
            {
            }

            if (f.ShowDialog() != DialogResult.OK)
                return;

            textBox2.Text = f.FileName;
        }

        private void close_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            UsbChoise uc = new UsbChoise();
            if (uc.ShowDialog(this) != DialogResult.OK)
                return;
            textBox1.Text = uc.DrivePath;
        }

        internal const int WM_DEVICECHANGE = 0x0219;

        protected override void WndProc(ref Message m)
        {
            if (m.Msg == WM_DEVICECHANGE)
            {
            }

            if (m.Msg == SystemMenu.WM_SYSCOMMAND)
            {
                switch (m.WParam.ToInt32())
                {
                    case WM_ABOUTBOX:
                        AboutBox a = new AboutBox();
                        a.ShowDialog();
                        break;
                }
            }

            base.WndProc(ref m);
        }

        private void UsbBurn_Load(object sender, EventArgs e)
        {
            textBox1.Text = (string)Application.UserAppDataRegistry.GetValue("DevicePath", "");
            textBox2.Text = (string)Application.UserAppDataRegistry.GetValue("ImagePath", "");
        }

        private void UsbBurn_FormClosing(object sender, FormClosingEventArgs e)
        {
            Application.UserAppDataRegistry.SetValue("DevicePath", textBox1.Text);
            Application.UserAppDataRegistry.SetValue("ImagePath", textBox2.Text);
        }
    }
}
