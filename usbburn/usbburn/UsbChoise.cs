﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Management;

namespace usbburn
{
    public partial class UsbChoise : Form
    {
        public string DrivePath;

        public UsbChoise()
        {
            InitializeComponent();
        }

        private void UsbChoise_Load(object sender, EventArgs e)
        {
            ManagementObjectCollection drives = new ManagementObjectSearcher("SELECT Caption, DeviceID FROM Win32_DiskDrive WHERE InterfaceType='USB'").Get();
            foreach (ManagementObject drive in drives)
            {
                listBox1.Items.Add(drive["DeviceID"]);
            }
        }

        private void OK_Click(object sender, EventArgs e)
        {
            DrivePath = listBox1.SelectedItem.ToString();
            DialogResult = DialogResult.OK;
        }

        private void listBox1_DoubleClick(object sender, EventArgs e)
        {
            if(listBox1.SelectedItem!=null)
                OK_Click(sender, e);
        }
    }
}
